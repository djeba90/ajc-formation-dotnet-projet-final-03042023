﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using twitch_simulation.Models;
using twitchDTO.api;

namespace twitch_simulation.Controllers
{
    public class StreamController : Controller
    {
        private DefaultDbContext context;
        private IMapper mapper;

        public StreamController(DefaultDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult<List<StreamDTO>>> Create(string value)
        {
            var titre = this.Request.Form["titreStream"];
            var dateDebut = this.Request.Form["dateDebut"];

            DateTime myDate = DateTime.ParseExact(dateDebut, "yyyy-MM-ddTHH:mm",
                                       System.Globalization.CultureInfo.InvariantCulture); //'2023-04-12T18:10

            var nstream = new StreamDTO(titre, myDate);
            var stream = mapper.Map<twitch.api.Stream>(nstream);
            this.context.Streams.Add(stream);
            await this.context.SaveChangesAsync();

            var streamAfficher = await context.Streams.ToListAsync();

            return View("Create", streamAfficher.Select(mapper.Map<StreamDTO>));
        }

        [HttpGet]
        public async Task<ActionResult<List<StreamDTO>>> Create()
        {
            var stream = await context.Streams.ToListAsync();

            return View("Create", stream.Select(mapper.Map<StreamDTO>));
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}
