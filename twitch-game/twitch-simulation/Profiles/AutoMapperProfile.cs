﻿using AutoMapper;
using twitchDTO.api;

namespace twitch_simulation.Profiles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<StreamDTO, twitch.api.Stream>();
            CreateMap<twitch.api.Stream, StreamDTO>();
        }
    }
}
