﻿using Microsoft.EntityFrameworkCore;

namespace twitch_simulation.Models
{
    public class DefaultDbContext : DbContext
    {
        public DefaultDbContext(DbContextOptions options) : base(options)
        {
        }

        protected DefaultDbContext()
        {
        }
        public DbSet<twitch.api.Stream> Streams { get; set; }
    }
}
