﻿using twitch.api.Adapters;

namespace twitch.infrastructure
{
    /// <summary>
    /// Classe implémentant l'interface servant à remplacer Console.Readline()
    /// </summary>
    public class ConsoleLecteur : ILecteurInfos
    {
        public string Lecture()
        {
            return Console.ReadLine();
        }
    }
}
