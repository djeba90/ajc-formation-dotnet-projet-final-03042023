﻿using twitch.api.Adapter;

namespace twitch.infrastructure
{
    /// <summary>
    /// Classe implémentant l'interface servant à remplacer Console.Write() et Console.WriteLine()
    /// </summary>
    public class ConsoleAfficheur : IAfficheur
	{ 
		public void AfficherAvecSaut(string message)
		{
			Console.WriteLine(message);
		}

		public void AfficherSansSaut(string message)
		{
			Console.Write(message);
		}

	}
}