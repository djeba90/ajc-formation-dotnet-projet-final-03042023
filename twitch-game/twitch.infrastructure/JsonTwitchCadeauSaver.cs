﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api;
using twitch.api.Adapter;

namespace twitch.infrastructure
{
	/// <summary>
	/// Classe implémentant l'interface pour sauvegarder en Json
	/// </summary>
	public class JsonTwitchCadeauSaver : ISave
	{

		#region Fields
		private readonly string chemin;
        private readonly Action<string> afficherInfo;
        private readonly Func<string> recupererInfo;
        #endregion

        public JsonTwitchCadeauSaver(Action<string> afficher, Func<string> recupererInfo)
        {
            this.afficherInfo = afficher;
            this.recupererInfo = recupererInfo;
        }

		public JsonTwitchCadeauSaver(string chemin)
		{
			this.chemin = chemin;
		}

		public void SaveAll(params Object[] objectToSave)
		{
			this.Save(objectToSave);
		}

		public void SaveOne(Object objectToSave)
		{
			this.Save(objectToSave);
		}

		public void Save(params object[] objectToJson)
		{

			var json = JsonConvert.SerializeObject(objectToJson, Newtonsoft.Json.Formatting.Indented);
            try
			{
				File.WriteAllText(this.chemin, json);
			}
			catch (IOException ex)
			{

				afficherInfo("DAS IST CASSÉ " + ex);
			}
		}
	}
}
