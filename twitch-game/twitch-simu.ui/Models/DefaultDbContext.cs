﻿using Microsoft.EntityFrameworkCore;
using twitch.api;

namespace twitch_simu.ui
{
    public class DefaultDbContext : DbContext
    {
        public DefaultDbContext(DbContextOptions options) : base(options)
        {
        }

        protected DefaultDbContext()
        {
        }
        public DbSet<twitch.api.Stream> Streams2 { get; set; }
    }
}
