﻿using twitch_simu.ui;
using twitchDTO.api;

namespace twitch_simu.ui.Models
{
    public class StreamSaver
    {
        public readonly DefaultDbContext context;

        public StreamSaver(DefaultDbContext context)
        {
            this.context = context;
        }

        public void Save(StreamDTO stream)
        {
            this.context.Add(stream);
            this.context.SaveChanges();
        }
    }
}
