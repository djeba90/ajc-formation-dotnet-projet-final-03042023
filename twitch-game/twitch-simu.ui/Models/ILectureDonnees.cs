﻿using twitchDTO.api;

namespace twitch_simu.ui.Models
{
    public interface ILectureDonnees<TData>
    {
        List<TData> GetAll();
    }
}
