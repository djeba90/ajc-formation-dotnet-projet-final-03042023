﻿using Microsoft.AspNetCore.Mvc;
using twitch_simu.ui.Models;
using twitch.api;
using twitchDTO.api;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace twitch_simu.ui.Controllers
{
    public class StreamController : Controller
    {
        private DefaultDbContext context;
        private IMapper mapper;

        public StreamController(DefaultDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        //[HttpPost]
        //public IActionResult Create(string name)
        //{
        //    var titre = this.Request.Form["name"];
        //    var dateDebut = this.Request.Form["dateDebut"];

        //    DateTime myDate = DateTime.ParseExact(dateDebut, "yyyy-MM-ddTHH:mm",
        //                               System.Globalization.CultureInfo.InvariantCulture); //'2023-04-12T18:10


        //    StreamSaver saver = new(this.context);
        //    saver.Save(new StreamDTO(new twitch.api.Stream(), myDate, titre));



        //    return View("Create");
        //}

        [HttpPost]
        public async Task<ActionResult<List<StreamDTO>>> Create(string value)
        {
            var titre = this.Request.Form["name"];
            var dateDebut = this.Request.Form["dateDebut"];

            DateTime myDate = DateTime.ParseExact(dateDebut, "yyyy-MM-ddTHH:mm",
                                       System.Globalization.CultureInfo.InvariantCulture); //'2023-04-12T18:10

            var nstream = new StreamDTO(titre, myDate);
            var stream = mapper.Map<twitch.api.Stream>(nstream);
            this.context.Streams2.Add(stream);
            await this.context.SaveChangesAsync();

            return View("Create");
        }

        [HttpGet]
        public async Task<ActionResult<List<StreamDTO>>> Create()
        {
            var stream = await context.Streams2.ToListAsync();

            return View("Create", stream.Select(mapper.Map<StreamDTO>));
        }


        //[HttpPost]
        //public IActionResult Add(string oui)
        //{
        //    string value = this.Request.Form["oui"];
        //    return RedirectToAction("list");
        //}

        //[HttpPost]
        //public IActionResult Add(PersonneAddViewModel item)
        //{
        //    //string value = this.Request.Form["oui"];
        //    this.context.Personnes.Add(item.UnePersonne);
        //    this.context.SaveChanges();
        //    return RedirectToAction("list");
        //}

    }
}