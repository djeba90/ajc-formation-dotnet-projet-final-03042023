﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
    /// <summary>
    /// Class contenant les différents choix du menu
    /// </summary>
    public class MenuItem
    {
        public MenuItem()
        {
            
        }

        public MenuItem(int id, string libelle, int ordreAffichage)
        {
            Id = id;
            Libelle = libelle;
            OrdreAffichage = ordreAffichage;
        }

        #region Methods
        public override string ToString()
        {
            return $"{this.Id} : {this.Libelle}";
        }
        #endregion

        #region Properties
        public int Id { get; set; }

        public string Libelle { get; set; } = "";

        public int OrdreAffichage { get; set; }
        #endregion
    }
}
