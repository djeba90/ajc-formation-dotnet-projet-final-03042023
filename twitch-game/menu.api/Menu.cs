﻿namespace menu.api
{
    /// <summary>
    /// Menu affiché dans la console
    /// </summary>
    public class Menu
    {
        #region Fields
        private readonly Action<string> afficherInfo;
        private readonly Func<string> recupererInfo;
        #endregion

        public Menu(Action<string> afficher, Func<string> recupererInfo)
        {
            this.afficherInfo = afficher;
            this.recupererInfo = recupererInfo;
        }

        #region Methods
        public void Afficher()
        {
            foreach (var item in this.Items.OrderBy(item => item.OrdreAffichage)) {
                afficherInfo(item.Id + " " + item.Libelle);
            }
        }
        #endregion

        #region Properties
        public List<MenuItem> Items { get; private set; } = new();
        #endregion
    }
}