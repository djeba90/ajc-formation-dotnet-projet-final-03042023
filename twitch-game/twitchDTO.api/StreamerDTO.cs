﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api;

namespace twitchDTO.api
{
    public class StreamerDTO
    {
        public string Prenom { get; set; }
        public string Pseudo { get; set; }
        public string Email { get; set; }
        public string? Image { get; set; }
        public string? ListAbonnes { get; set; }
        public string? ListStream { get; set; }
        public string? ListViewers { get; set; }
        public string? ListTypeCadeau { get; set; }
    }
}
