﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using twitch.api;

namespace twitchDTO.api
{
    public class StreamDTO
    {
        public StreamDTO()
        {
            
        }

        public StreamDTO(string titre, DateTime dateDebut)
        {
            Titre = titre;
            DateDebut = dateDebut;
        }

        public string? Titre { get; set; } = "null en bdd";
        public DateTime ?DateDebut { get; set; }
    }
}
