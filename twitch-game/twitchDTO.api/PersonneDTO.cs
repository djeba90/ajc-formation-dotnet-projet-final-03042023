﻿using Newtonsoft.Json;
using System.Text.Json.Serialization;
using twitch.api;

namespace twitchDTO.api
{
    public class PersonneDTO
    {
        public string Prenom { get; set; }
        public string Pseudo { get; set; }
        public string Email { get; set; }
        public string? Image { get; set; }
        public string? ListAbonnement { get; set; }
        public string? NomQuantiteKdo { get; set; }
        public bool IsConnected { get; set; } = false;

    }
}