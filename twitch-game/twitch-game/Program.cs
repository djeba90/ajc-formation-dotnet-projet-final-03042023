﻿using menu.api;
using twitch.api;
using twitch.api.Adapter;
using twitch.api.Adapters;
using twitch.infrastructure;

IAfficheur afficheur = new ConsoleAfficheur();
ILecteurInfos lecteurInfos = new ConsoleLecteur();

var path = Path.Combine(Environment.CurrentDirectory, "ici.json");
ISave saver = new JsonTwitchCadeauSaver("./ici.json");
Menu menu = new(mess =>
{
    Console.ForegroundColor = ConsoleColor.Magenta;
    afficheur.AfficherAvecSaut(mess);
    Console.ForegroundColor = ConsoleColor.White;
},
lecteurInfos.Lecture);

menu.Items.Add(new MenuItem(1, "Ajouter un nouveau abonne", 1));
menu.Items.Add(new MenuItem(2, "Liste des cadeaux", 2));
menu.Items.Add(new MenuItem(3, "Ajouter un cadeau", 3));
menu.Items.Add(new MenuItem(4, "Ajouter des stream", 3));
menu.Items.Add(new MenuItem(5, "Sauvegarder", 4));

menu.Afficher();
Streamer streamer = new Streamer(afficheur.AfficherAvecSaut, lecteurInfos.Lecture);
List<Personne> lesABOS = new List<Personne>
{
    new Personne(1, "FRANCOIS", "MULLER", "MULLER@"),
    new Personne(2, "BAPT", "ISTE", "IST@")
};
twitch.api.Stream stream = new("DJEBA", streamer, afficheur.AfficherAvecSaut, saver, lecteurInfos.Lecture, lesABOS);
stream.ChoixMethode();
