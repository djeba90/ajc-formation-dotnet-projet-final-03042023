﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    /// <summary>
    /// Class contenant de type des cadeaux
    /// </summary>
    public class TypeCadeau
    {
        #region Constructors
        public TypeCadeau(string name)
        {
            this.Name = name;
        }
        #endregion

        #region Properties
        public int Id { get; set; }
        public string Name { get; set; }
        #endregion
    }
}
