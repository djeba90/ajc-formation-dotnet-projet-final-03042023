﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api.Adapter
{
    /// <summary>
    /// Interface servant à sauvegarder
    /// </summary>
    public interface ISave
	{
		public void SaveOne(Object objectToSave);
		public void SaveAll(params Object[] objectToSave);

	}
}
