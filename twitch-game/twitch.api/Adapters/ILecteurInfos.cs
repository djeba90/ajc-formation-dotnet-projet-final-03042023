﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api.Adapters
{
    /// <summary>
    /// Interface servant à remplacer Console.Readline()
    /// </summary>
    public interface ILecteurInfos
    {
        string Lecture();
    }
}


