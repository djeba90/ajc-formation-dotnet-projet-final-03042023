﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api.Adapter
{
    /// <summary>
    /// Interface servant à remplacer Console.Write() et Console.WriteLine()
    /// </summary>
    public interface IAfficheur
	{
		public void AfficherAvecSaut(String message);
		public void AfficherSansSaut(String message);
	}
}
