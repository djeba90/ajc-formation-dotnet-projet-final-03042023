﻿using Newtonsoft.Json;
using twitch.api.Adapter;

namespace twitch.api
{
    /// <summary>
    /// Stream, class qui fait les appels en fonction du menu
    /// </summary>
    public class Stream
    {
        #region Fields
        public int id;
        public string titre;
        public DateTime dateDebut = DateTime.Now;
        public DateTime dateFin;
        public Streamer streamer;
        public List<Cadeau> cadeauList = new List<Cadeau>();
        public readonly Action<string> afficherAvecSaut;
		public readonly ISave saver;
		private readonly Func<string> recupererSaisie;
        #endregion

        #region Constructors
        public Stream() { }
        public Stream(string titre, DateTime dateDebut)
        {
            this.Titre = titre;
            this.DateDebut = dateDebut;
        }
		public Stream(List<Personne> viewers)
        {
            this.ListViewers = viewers;
        }
        public Stream(string titre, Streamer streamer, Action<string> afficherAvec, ISave saveCadeau, Func<string> recupererSaisie, List<Personne> viewers)

        {

            this.Titre = titre;
            this.DateDebut = DateTime.Now;
            this.streamer = streamer;
			this.afficherAvecSaut = afficherAvec;
			this.saver = saveCadeau;
            this.recupererSaisie = recupererSaisie;
            this.ListViewers = viewers;
		}
        #endregion

        #region Methods
        public void GiftKdo()
        {
			Random gift = new Random();
            afficherAvecSaut("Salut quel cadeau veux tu offrir?");
			string kdoSaisie = this.recupererSaisie().ToUpper();
			afficherAvecSaut("Et combien de kdo veux tu offrir?");
			string qteSaisie = this.recupererSaisie();
			TypeCadeau typeCadeau = new(kdoSaisie);
			DateTime dat1 = new DateTime();
            for(int i = 0; i< int.Parse(qteSaisie); i++)
            {
                if(this.streamer.ListAbonnes != null)
                {
                    foreach (Personne pers in JsonConvert.DeserializeObject<List<Personne>>(this.streamer.ListAbonnes))
                    {
                        if (gift.Next(1, 10) == 5)
                        {
                            Cadeau newcadeau = new(gift.Next(1, 1000000), pers, typeCadeau, dat1);
                            cadeauList.Add(newcadeau);
                        }
                        else
                        {
                            //Exemple c'est pour montrer que ça fonctionne
                            Cadeau newcadeau = new(gift.Next(1, 1000000), pers, typeCadeau, dat1);
                            cadeauList.Add(newcadeau);
                        }
                    }
                }

            }
        }

		public void AfficheCadeaux()
		{
			foreach (var item in cadeauList)
			{
                afficherAvecSaut(item.TypeCadeauGifted.Name);
			}
		}

		public void SauvegardeCadeauxEtAbonnes()
		{
			saver.SaveAll(cadeauList, JsonConvert.DeserializeObject<List<Personne>>(this.streamer.ListAbonnes));

		}
        public void AddFollower()
        {
            this.streamer.AddFollower();
        }
        public void AddViewers()
        {
            afficherAvecSaut("Salut combien de viewers veux tu ajouter à ton stream?");
            int nbrAbo = int.Parse(this.recupererSaisie());
            for(int i = 0;i< nbrAbo; i++)
            {
                ListViewers.Add(new Personne("Francois"+ i));
            }
        }
        public void AddStream()
        {
            this.streamer.AddStream();
        }
        public void ChoixMethode()
        {
            string saisie;

            do
            {
				afficherAvecSaut("Select a option");
				saisie = this.recupererSaisie().ToUpper();
                Dictionary<String, Action> dict = new Dictionary<String, Action>()
                {
                    {"1",AddFollower},
                    {"2",AfficheCadeaux},
                    {"3",GiftKdo},
                    {"4",AddStream},
                    {"5",SauvegardeCadeauxEtAbonnes}
                };
                dict.TryGetValue(saisie, out var actionReturn);
                actionReturn();

            } while (saisie != "STOP");

		}
        #endregion

		#region Properties
		public int Id { get ; set ; }
        public string Titre { get; set; }
        public DateTime DateDebut { get; set; }
        public DateTime ?DateFin { get; set; }
        public Streamer ?Streamer { get; set; }
        public List<Personne> ?ListAbonnes { get; set; } = new List<Personne>();
        public List<Personne> ?ListViewers { get; set; } = new List<Personne>();
        #endregion
    }
}
