﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace twitch.api
{
    /// <summary>
    /// Permet de créer un cadeau avec un type et un abonné 
    /// </summary>
    public class Cadeau
    {
        Personne aboLinked;
        TypeCadeau typeCadeauGifted;

        public Cadeau(int id, Personne aboLinked, TypeCadeau typeCadeauGifted, DateTime DateExpiration)
        {
            this.Id = id;
            this.AboLinked = aboLinked;
            this.TypeCadeauGifted = typeCadeauGifted;
            this.DateExpiration = DateExpiration;
        }

        #region Properties
        public int Id { get; set; }
        public Personne AboLinked { get => aboLinked; set => aboLinked = value; }
        public TypeCadeau TypeCadeauGifted { get => typeCadeauGifted; set => typeCadeauGifted = value; }
        public DateTime DateExpiration { get; set; }
        #endregion
    }
}
