﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using twitch.api.Adapter;

namespace twitch.api
{
    /// <summary>
    /// Streamer, peut ajouter un stream et/ou des viewers
    /// </summary>
    public class Streamer
    {
        #region Constructors
        public Streamer(int id, string prenom, string pseudo, string email) {

            Id = id;
            Prenom = prenom;
            Pseudo = pseudo;
            Email = email;
        }
        public Streamer(int id)
        {
            Id = id;
        }
        public Streamer()
        {

        }
        public Streamer(Action<string> afficherAvec, Func<string> recupererSaisie)
        {
			this.afficherAvecSaut = afficherAvec;
			this.recupererSaisie = recupererSaisie;
		}
        #endregion

        #region Methods
        public void AjoutCadeau(string nomCadeau)
        {
            //ListTypeCadeau.Add(new TypeCadeau(nomCadeau));
        }

        public void AddFollower()
        {
			afficherAvecSaut("Salut quel pseudo veux tu donner?");
			string pseudoSaisie = this.recupererSaisie();

            List<Personne> vraieListAbo = new List<Personne>(); ;
            if (ListAbonnes != null)
            {
                vraieListAbo = JsonConvert.DeserializeObject<List<Personne>>(ListAbonnes);
            }
            vraieListAbo.Add(new Personne(pseudoSaisie));
            ListAbonnes = JsonConvert.SerializeObject(vraieListAbo);
            
        }
        public List<Personne> AddViewers(int nbrViewers)
        {
            List<Personne> vraieListViewers = new List<Personne>();
            for (int i = 0; i < nbrViewers; i++)
            {
                vraieListViewers.Add(new Personne("Francois" + i));
            }
            return vraieListViewers;
        }
        public void AddStream()
        {
            afficherAvecSaut("Salut combien de stream veux tu ajouter");
            int nbrStream = int.Parse(this.recupererSaisie());
            List<Stream> vraieListStream = new List<Stream>();
            for (int i = 0; i < nbrStream; i++)
            {
                if (ListViewers != null)
                {
                    vraieListStream = JsonConvert.DeserializeObject<List<Stream>>(ListStream);
                }
                afficherAvecSaut("Salut combien de viewers veux tu ajouter à ce stream?");
                int nbrViewers = int.Parse(this.recupererSaisie());
                vraieListStream.Add(new(this.AddViewers(nbrViewers)));
                ListStream = JsonConvert.SerializeObject(vraieListStream);
            }
            afficherAvecSaut(ListStream);
        }
        public void Demarrer(Stream stream, List<Personne> viewers)
        {
            //ListStream.Add(stream);
        }
        #endregion

        #region Properties
        public int Id { get; set; }
        public string Prenom { get; set; }
        public string Pseudo { get; set; }
        public string Email { get; set; }
        public string? Image { get; set; }

        public string? ListAbonnes { get; set; }
        public string? ListStream { get; set; }
        public string? ListViewers { get; set; }
        public string? ListTypeCadeau { get; set; }

        public readonly Action<string> afficherAvecSaut;

		public readonly Func<string> recupererSaisie;
		#endregion
	}
}
