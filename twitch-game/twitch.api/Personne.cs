﻿using System.ComponentModel.DataAnnotations.Schema;

namespace twitch.api
{
    /// <summary>
    /// Personne qui peut regarder un stream et être abonné
    /// </summary>
    public class Personne
    {
        //Dictionary<string, int> _NomQuantiteKdo = new Dictionary<string, int>();
    
        #region Constructors
        public Personne()
        {
            
        }
        public Personne(int id)
        {
            Id = id;
        }
        public Personne(string pseudo)
        {
            Pseudo = pseudo;
        }
        public Personne(int id, string prenom, string pseudo, string email)
        {
            Id = id;
            Prenom = prenom;
            Pseudo = pseudo;
            Email = email;
        }

        public Personne(int id, string prenom, string pseudo, string email, string image, string listAbonnement, string nomQuantiteKdo)
        {
            Id = id;
            Prenom = prenom;
            Pseudo = pseudo;
            Email = email;
            Image = image;
            ListAbonnement = listAbonnement;
            NomQuantiteKdo = nomQuantiteKdo;
        }
        #endregion


        #region Properties
        public int Id { get; set; }
        public string Prenom { get; set; } = "francois";
        public string Pseudo { get; set; }
        public string Email { get; set; } = "francois@gmail.com";
        public string ?Image { get; set; }
        public string ?ListAbonnement { get; set; }
        public string ?NomQuantiteKdo { get; set; }
        public bool IsConnected { get; set; } = false;

        //public List<Streamer> ListAbonnement { get; set; } = new List<Streamer>();
        //public Dictionary<string, int> NomQuantiteKdo { get => _NomQuantiteKdo; set => _NomQuantiteKdo = value; }
        #endregion
    }
}