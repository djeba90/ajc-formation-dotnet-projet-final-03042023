﻿using AutoMapper;
using twitch.api;
using twitchDTO.api;

namespace twitch_game.ui.Profiles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Streamer, StreamerDTO>();
            CreateMap<Personne, PersonneDTO>();
        }
    }
}
