﻿using Microsoft.AspNetCore.Mvc;
using twitch.ui.Models;
using twitch.api;
using twitchDTO.api;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Azure.Core;
using Newtonsoft.Json;
using System.Runtime.CompilerServices;

namespace twitch.ui.Controllers
{
    public class StreamerController : Controller
    {
        private DefaultDbContext context;
        private IMapper mapper;

        public StreamerController(DefaultDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

/*        [HttpPost]
        public IActionResult Filtre(string value)
        {
            string optionChoisie = this.Request.Form["Option"];

            Console.WriteLine("-------------------------  " + optionChoisie + "  -------------------------");
            var requete = new DbStreamerLecteur(this.context);
            var modelAEnvoyerALaView = requete.GetAll();
            if (optionChoisie == "2")
            {
                modelAEnvoyerALaView = requete.GetActif();
            }



            return View("Filtre", modelAEnvoyerALaView);
        }

        [HttpGet]
        public IActionResult Filtre()
        {
            Console.WriteLine("SALUT");
            var requete = new DbStreamerLecteur(this.context);
            var modelAEnvoyerALaView = requete.GetAll();
            return View("Filtre", modelAEnvoyerALaView);
        }*/
        [HttpPost]
        public async Task<ActionResult<List<StreamerDTO>>> Filtre(string value)
        {
            string optionChoisie = this.Request.Form["Option"];
            var streamer = await context.Streamers.ToListAsync();
            List<Personne> listViewers = new List<Personne>();
            int nbrStream = 0;
            //streamer = streamer.FindAll(e => e.Prenom == "OUI");
            foreach (var item in streamer)
            {
                if(item.ListStream != null)
                {
                List<twitch.api.Stream> stream = JsonConvert.DeserializeObject<List<twitch.api.Stream>>(item.ListStream);
                    foreach (var streamUnique in stream)
                    {
                        nbrStream++;
                        foreach (var viewer in streamUnique.ListViewers)
                        {
                            //On récupère les viewers de tous les streams
                            listViewers.Add(viewer);
                        }
                    }
                }
            }
            if (optionChoisie == "2")
            {
               List<String> pseudoList = new List<String>();    
               foreach(var item in listViewers)
                {
                    pseudoList.Add(item.Pseudo);
                }
                var q = from x in pseudoList
                        group x by x into g
                        let count = g.Count()
                        orderby count descending
                        where count > (nbrStream * 0.7)
                        select g.Key;
                listViewers.Clear();
                foreach(var item in q)
                {
                    listViewers.Add(new(item));
                }
            }
            return View("Filtre", listViewers.Select(mapper.Map<PersonneDTO>));
        }


        [HttpGet]
        public async Task<ActionResult<List<StreamerDTO>>> Filtre()
        {
            var streamer = await context.Streamers.ToListAsync();
            List<Personne> listViewers = new List<Personne>();
            foreach (var item in streamer)
            {
                
                if (item.ListStream != null)
                {
                    List<twitch.api.Stream> stream = JsonConvert.DeserializeObject<List<twitch.api.Stream>>(item.ListStream);
                    foreach (var streamUnique in stream)
                    {
                        foreach (var viewer in streamUnique.ListViewers)
                        {
                            //On récupère les viewers de tous les streams
                            listViewers.Add(viewer);
                        }
                    }
                }
            }
            listViewers = listViewers.Distinct().ToList();

            return View("Filtre", listViewers.Select(mapper.Map<PersonneDTO>));
        }

        public IActionResult Edit()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        //[HttpPost]
        //public IActionResult Add(string oui)
        //{
        //    string value = this.Request.Form["oui"];
        //    return RedirectToAction("list");
        //}

        //[HttpPost]
        //public IActionResult Add(PersonneAddViewModel item)
        //{
        //    //string value = this.Request.Form["oui"];
        //    this.context.Personnes.Add(item.UnePersonne);
        //    this.context.SaveChanges();
        //    return RedirectToAction("list");
        //}

    }
}