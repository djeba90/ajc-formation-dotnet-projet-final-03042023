﻿using Microsoft.AspNetCore.Mvc;
using twitch.ui.Models;
using twitch.api;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using twitchDTO.api;

namespace twitch.ui.Controllers
{
    public class PersonneController : Controller
    {
        private DefaultDbContext context;
        private IMapper mapper;

        public PersonneController(DefaultDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }
        public async Task<ActionResult<List<PersonneDTO>>> List()
        {
            List<Personne> persoConnect = new List<Personne>();
            var personne = await context.Personnes.ToListAsync();
            personne.ForEach(x => { if (x.IsConnected) { persoConnect.Add(x); } });
            return View("List", persoConnect.Select(mapper.Map<PersonneDTO>));
        }
        public IActionResult Edit()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        //[HttpPost]
        //public IActionResult Add(string oui)
        //{
        //    string value = this.Request.Form["oui"];
        //    return RedirectToAction("list");
        //}

        //[HttpPost]
        //public IActionResult Add(PersonneAddViewModel item)
        //{
        //    //string value = this.Request.Form["oui"];
        //    this.context.Personnes.Add(item.UnePersonne);
        //    this.context.SaveChanges();
        //    return RedirectToAction("list");
        //}

    }
}