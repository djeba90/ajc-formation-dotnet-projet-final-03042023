﻿using Microsoft.EntityFrameworkCore;
using twitch.api;

namespace twitch.ui
{
    public class DefaultDbContext : DbContext
    {
        public DefaultDbContext(DbContextOptions options) : base(options)
        {
        }

        protected DefaultDbContext()
        {
        }
        public DbSet<twitch.api.Personne> Personnes { get; set; }
        public DbSet<twitch.api.Streamer> Streamers { get; set; }
    }
}
