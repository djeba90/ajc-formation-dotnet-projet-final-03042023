﻿using twitch.ui;
using twitchDTO.api;

namespace twitch.ui.Models
{
    public class PersonneSaver
    {
        private readonly DefaultDbContext context;

        public PersonneSaver(DefaultDbContext context)
        {
            this.context = context;
        }

        public void Save(PersonneDTO personne)
        {
            // ajouter conversion json pour list et dico
            this.context.SaveChanges();
        }
    }
}
